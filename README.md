# Validador de CSV



## Exercício solicitado pela empresa Effetive Sistemas para o processo seletivo

Este projeto é um leitor feito em Java que lê arquivos CSV, verifica se os mesmos possuem um certo número
de colunas e copia-os para uma pasta "Válida" ou "Inválida", dependendo do resultado do teste.

## Como iniciar

Dentro da pasta src do projeto, existem 3 pastas, chamadas "pastaInvalido", "pastaPendente" e "pastaValido".
A pasta pendente contém 2 arquivos, um que passa a validação e outro que falha, enquanto as outras duas pastas 
encontram-se vazias. Ao iniciar o programa, o programa solicitará ao usuário os caminhos absolutos das 3 pastas, 
e então iniciará o processo de validação, informando os arquivos validados, assim como o resultado da validação.
Abaixo encontra-se um exemplo do programa sendo iniciado.

```
Bem vindo ao Sistema de Validação de CSV!

Informe o local da pasta pendentes: 
C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\src\pastaPendente

Informe o local da pasta validos: 
C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\src\pastaValido

Informe o local da pasta invalidos: 
C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\src\pastaInvalido
Iniciando o processo de validação, detectando arquivos CSV na pasta informada
O sistema encontrou 2 arquivos, iniciando validação
Iniciando a validação do arquivo de nome: arquivo.csv
Arquivo válido, copiado para a pasta de caminhos válidos
Iniciando a validação do arquivo de nome: arquivoInvalido.csv
Arquivo inválido, copiado para a pasta de caminhos inválidos
Todos os arquivos foram validados!
```

Após o término da execução, os arquivos já validados poderão ser encontrados nas duas pastas antes vazias,
aqueles que passaram estarão na pasta validos, os que falharam estarão na pasta invalidos.

## Executando o .JAR

O .jar compilado encontra-se na pasta out, juntamente com as pastas e os arquivos .csv, tal como o exemplo acima.
Para iniciar o programa, abra um terminal de sua escolha e inicie o programa com o comando java -jar. Após isso, o
funcionamento do programa se dará de maneira igual ao exemplo anterior. Abaixo um exemplo do .jar sendo executado:

```
PS C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\out> java -jar .\ValidadorCSV.jar
Bem vindo ao Sistema de Validação de CSV!

Informe o caminho absoluto da pasta pendentes:
C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\out\pastaPendente

Informe o caminho absoluto da pasta válidos:
C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\out\pastaValido

Informe o caminho absoluto da pasta inválidos:
C:\Users\pejo5\Desktop\Projeto da Effective\validador-de-csv\out\pastaInvalido
Iniciando o processo de validação, detectando arquivos CSV na pasta informada
O sistema encontrou 2 arquivos, iniciando validação
Iniciando a validação do arquivo de nome: arquivo.csv
Arquivo válido, copiado para a pasta de caminhos válidos
Iniciando a validação do arquivo de nome: arquivoInvalido.csv
Arquivo inválido, copiado para a pasta de caminhos inválidos
Todos os arquivos foram validados!
```

##Validação de campos

Agora, o leitor de CSV também irá ler as linhas de todos os arquivos para verificar
a validez dos campos. Ele validará se o campo de preço é válido, se o nome do cliente
foi informado, se a data é válida e se o preço é válido. Junto do projeto, irão mais
alguns arquivos .CSV que exemplificam quais e como as funcionalidades de validação funcionam.
Abaixo está um exemplo de como o código se comporta:

```
"C:\Program Files\Java\jdk-19\bin\java.exe" -agentlib:jdwp=transport=dt_socket,address=127.0.0.1:57408,suspend=y,server=n -javaagent:C:\Users\pejo5\AppData\Local\JetBrains\IdeaIC2022.2\captureAgent\debugger-agent.jar -Dfile.encoding=UTF-8 -Dsun.stdout.encoding=UTF-8 -Dsun.stderr.encoding=UTF-8 -classpath "C:\Users\pejo5\Documents\VagaEffectiveSistemas\Projeto da Effective\validador-de-csv\out\production\validador-de-csv;C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2.3\lib\idea_rt.jar" Main
Connected to the target VM, address: '127.0.0.1:57408', transport: 'socket'
Bem vindo ao Sistema de Validação de CSV!

Informe o caminho absoluto da pasta pendentes: 
C:\Users\pejo5\Documents\VagaEffectiveSistemas\Projeto da Effective\validador-de-csv\src\pastaPendente

Informe o caminho absoluto da pasta válidos: 
C:\Users\pejo5\Documents\VagaEffectiveSistemas\Projeto da Effective\validador-de-csv\src\pastaValido

Informe o caminho absoluto da pasta inválidos: 
C:\Users\pejo5\Documents\VagaEffectiveSistemas\Projeto da Effective\validador-de-csv\src\pastaInvalido
Iniciando o processo de validação, detectando arquivos CSV na pasta informada
O sistema encontrou 7 arquivos, iniciando validação
Iniciando a validação do arquivo de nome: arquivo.csv
Arquivo válido, copiado para a pasta de caminhos válidos
Iniciando a validação do arquivo de nome: arquivoInvalido.csv
Arquivo inválido, copiado para a pasta de caminhos inválidos
Iniciando a validação do arquivo de nome: arquivoInvalidoDataInvalida.csv
Uma ou mais colunas do documento estão inválidas, favor verificar o CSV!
Arquivo inválido, copiado para a pasta de caminhos inválidos
Iniciando a validação do arquivo de nome: arquivoInvalidoPrecoInvalido.csv
Uma ou mais colunas do documento estão inválidas, favor verificar o CSV!
Arquivo inválido, copiado para a pasta de caminhos inválidos
Iniciando a validação do arquivo de nome: arquivoInvalidoSemNomeLinha3.csv
Arquivo inválido, copiado para a pasta de caminhos inválidos
Iniciando a validação do arquivo de nome: arquivoInvalidoSemUmCampo.csv
Arquivo inválido, copiado para a pasta de caminhos inválidos
Iniciando a validação do arquivo de nome: arquivoValido.csv
Arquivo válido, copiado para a pasta de caminhos válidos
Todos os arquivos foram validados!
```